/*
  insertion-sort - 插入排序算法
 */

#include<stdio.h>

#define LENGTH 30

void insertionSort(int [], int);
void insertionSortDown(int [], int);

int main(void)
{
	int A[LENGTH];
	int length;

	printf("输入要排序序列的长度：");
	scanf("%d", &length);
	printf("输入需要排序的序列：");
	for (int i = 0; i < length; i++)
	{
		scanf("%d", &A[i]);
	}

	// 对无序序列进行排序
	insertionSort(A, length);
	
	// 输出进行插入排序后的序列
	printf("经过排序后的序列：");
	for (int i = 0; i < length; i++)
	{
		printf("%d ", A[i]);
	}

	insertionSortDown(A, length);
	printf("经过排序后的序列：");
	for (int i = 0; i < length; i++)
	{
		printf("%d ", A[i]);
	}
	
	return 0;
}

void insertionSort(int A[], int n)
{
		
	for (int j = 1; j < n; j++) 
	{
		int key = A[j];
		int i = j - 1;
		while (i >= 0 & A[i] > key)
		{
			A[i + 1] = A[i];
			i = i - 1;
		}
		A[i+1] = key;
	}
}

void insertionSortDown(int A[], int n)
{
	for (int j = 1; j < n; j++) 
	{
		int key = A[j];
		int i = j - 1;
		while (i >= 0 & A[i] < key)
		{
			A[i + 1] = A[i];
			i = i - 1;
		}
		A[i+1] = key;
	}
}
