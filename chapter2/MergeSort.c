/*
 MergeSort.c - Merge sort
*/

#include<stdio.h>

#define INFINITE 10000000
#define LENGTH 100

void merge_sort(int [], int, int);
void merge(int[], int, int, int);

int main(void)
{
	int length;
	int A[LENGTH];

	printf("Enter the length:");
	scanf("%d", &length);
	
	printf("Enter the numbers:");
	for (int i = 0; i < length; i++)
	{
		scanf("%d", &A[i]);
	}

	merge_sort(A, 0, length);
	
	printf("Numbers after merge sort:");
	for (int i = 0; i < length; i++)
	{
		printf("%d ", A[i]);
	}	
	
	return 0;
}

void merge_sort(int A[], int p, int r)
{
	int q;

	if (p < r)
	{
		q = (p + r) / 2;
		merge_sort(A, p, q);
		merge_sort(A, q+1, r);
		merge(A, p, q, r);
	}
}

void merge(int A[], int p, int q, int r)
{
	int n1 = q - p + 1;
	int n2 = r - q;
	int L[n1], R[n2];
	int i, j;
	
	for (int i = 0; i < n1; i++)
	{
		L[i] = A[p + i];
	}
	for (int i = 0; i < n2; i++)
	{
		R[i] = A[q + 1 + i];
	}
	L[n1] = INFINITE;
	R[n2] = INFINITE;
	
	i = 0;
	j = 0;
	for(int k = p; k <r; k++)
	{
		if (L[i] <= R[j])
		{
			A[k] = L[i];
			i += 1;
		}
		else
		{
			A[k] = R[j];
			j += 1;
		}
	}
}
