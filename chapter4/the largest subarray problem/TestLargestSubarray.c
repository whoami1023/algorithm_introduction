/*
 TestLargestSubarray.c -- Testing the program wether true or false
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "LargestSubarray.h"

#define DEBUG 0
#define TEST_FIND_MAX_CROSSING_SUBARRAY 0

int main(void)
{
	int A[MAXLEN];
	int len;
	memset(&A, 0, sizeof(A));
	int *res;
	res = (int*)malloc(sizeof(int) * 3);
#if DEBUG == 1
	res[0] = 1;
	res[1] = 2;
	res[2] = 3;
#endif	
	/* Enter the length of sequence */
	printf("[Inputing >>>]\n");
	printf("Enter the length of sequence: ");
	scanf("%d", &len);
	/* Enter the sequence */
	printf("[Inputing >>>]\n");
	printf("Enter sequence: ");
	for (int i = 1; i <= len; i++)
	{
		scanf("%d", &A[i]);
	}
#if TEST_FIND_MAX_CROSSING_SUBARRAY == 1
	/* Get the result */
	res = find_max_crossing_subarray(A, 1, (1 + len) / 2, len);
#else
	res = find_maximum_subarray(A, 1, len);
#endif	
	/* Output the result */
	printf("[Outputing >>>]\n");
	printf("[Left index] >>> %d\n", res[0]);
	printf("[Right index] >>> %d\n", res[1]);
	printf("[Max value] >>> %d\n", res[2]);
	free(res);
	return 0;
}
