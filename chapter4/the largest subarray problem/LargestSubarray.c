/*
 LargetSubarray.c -- Inplementing the definition of LargestSubarray.h
*/

#include "LargestSubarray.h"
#include <stdlib.h>

#define INNIFITE 10000

int* find_max_crossing_subarray(int A[], int low, int mid, int high)
{
	int left_sum = -INNIFITE, right_sum = -INNIFITE;
	int left_index, right_index;
	int sum = 0;
	int *result;	/* result[0]->left_index, result[1]->right_index, result[2]->right_sum+left_sum */
	result = (int *)malloc(sizeof(int) * 3);

	/*
	int restult[3];
	local variable, after call function, the variable was destroyed.
	*/
	
	/* Computing the constinus the max of subarray on the left of mid */
	for (int i = mid; i >= low; i--)
	{
		sum += A[i];
		if (sum > left_sum)
		{
			left_sum = sum;
			left_index = i;
		}
	}
	
	/* Computing the constnue the max of subaray on right of the mid*/
	sum = 0;
	for (int i = mid + 1; i <= high; i++) 
	{
		sum += A[i];
		if (sum > right_sum)
		{
			right_sum = sum;
			right_index = i;
		}
	}

	result[0] = left_index;
	result[1] = right_index;
	result[2] = right_sum + left_sum;

	return result;
}

int* find_maximum_subarray(int A[], int low, int high)
{
	int * result;
	int *left, *right, *cross;
	int mid;
	result = (int *)malloc(sizeof(int) * 3);
	left = (int *)malloc(sizeof(int) * 3);
	right = (int *)malloc(sizeof(int) * 3);
	cross = (int *)malloc(sizeof(int) * 3);
	
	if (low == high)
	{
		result[0] = low;
		result[1] = high;
		result[2] = A[low];
		return result;
	}
	else
	{
		mid = (low + high) / 2;
		left = find_maximum_subarray(A, low, mid);
		right = find_maximum_subarray(A, mid+1, high);
		cross = find_max_crossing_subarray(A,low,mid,high);
		if (left[2] > right[2] && left[2] > cross[2])
			return left;
		else if (right[2] > left[2] && right[2] > cross[2])
			return right;
		else
			return cross;
	}
}
