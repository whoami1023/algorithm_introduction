/*
 LargestSubarray.h -- The definition of the largest subarray problem
*/

#ifndef _LARGESTSUBARRAY_H_
#define _LARGESTSUBARRAY_H_
#define MAXLEN 50

int* find_max_crossing_subarray(int A[], int low, int mid, int high);
int* find_maximum_subarray(int A[], int low, int high);

#endif
