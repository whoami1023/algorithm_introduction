/*
 LinkedList.c - The inplemention of linked list
*/
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include"LinkedList.h"

bool list_is_empty(const LinkedList *L)
{
	return L->items == 0;
}

Node* list_search(const LinkedList *L, const Item item)
{
	Node *p;
	p = L->head;
	while (p != NULL && p->item != item)
		p = p->next;
	return p;
}

/*
 list_insert(): Inserting the value in the front of head
*/

void list_insert(LinkedList *L, Item item)
{
	Node *p;
	p = (Node *)malloc(sizeof(Node));
	p->item = item;
	p->next = L->head;
	L->head = p;
	L->items++;
}

void print_list(const LinkedList *L)
{
	Node *p;
	if (list_is_empty(L))
	{
		fprintf(stderr, "[ERROR]:The linked list is empty!");
		exit(1);
	}
	p = L->head;
	while (p != NULL)
	{
		printf("%d ", p->item);
		p = p->next;
	}
	printf("\n");
}
void list_delet(LinkedList *L, Item item)
{
	Node *p, *q;
	p = list_search(L, item);
	if (!p)
	{
		fprintf(stderr, "[ERROR]:Can't find the key!");
		exit(1);
	}
	q = L->head;
	while(q->next != p)
	{
		q = q->next;
	}
	q->next = p->next;
	free(p);	
}
