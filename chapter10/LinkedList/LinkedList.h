/*
 LinkedList.h -- The definition of linked list
*/

#ifndef _Linked_List_H_
#define _Linked_List_H_

#include<stdbool.h>

typedef int Item;

typedef struct node {
	Item item;
	struct node *next;
} Node;

typedef struct linkedlist {
	Node *head;
	int items; // The number of items in linked list;
} LinkedList;

Node* list_search(const LinkedList *L, const Item item);
bool list_is_empty(const LinkedList *L);
void list_insert(LinkedList *L, Item item);
void print_list(const LinkedList *L);
void list_delet(LinkedList *L, Item item);

#endif
