/*
 UseLinkedList.c - Testing the interface of linked list
*/

#include<stdio.h>
#include"LinkedList.h"
#include<stdlib.h>

int main(void)
{
	LinkedList *L;
	L = (LinkedList *)malloc(sizeof(LinkedList));
	list_insert(L, 2);
	list_insert(L, 3);
	printf("[After inserting]:");
	print_list(L);
	list_delet(L, 2);
	printf("[After deleting]:");
	print_list(L);
	return 0;
}
