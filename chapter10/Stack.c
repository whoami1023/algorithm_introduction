/*
 Stack.c - Using array to inplement stack
*/

#include <stdio.h>

#define LENGTH 50

struct Stack{
	int s[LENGTH];
	int top;
};

void push(Stack S, int num); // pushing a number to stack
void pop(Stack S); // pop a number from the top if stack
bool empty(Stack S); // adjust the stack is empty
void printStack(Stack S); // Print the numbers of stack

int main(void)
{
	struct Stack S;
	S.top = -1;
	push(S, 2);
	push(S, 3);
	printf("[After pushing]:");
	printStack(S);
	pop(S);
	printf("[After Poping]:");
	printStack(S);
	return 0;
} 

void push(Stack S, int num)
{
	S.top += 1;
	S.s[S.top] = num; 
}

void pop(Stack S)
{
	if (empty(S))
		printf("[Error]:The stack is empty!");
		exit(0);
	else
		S.top--;
}

bool empty(Stack s)
{
	if (S.top == -1)
		return true;
	return false;
}

void printStack(Stack S)
{
	for (int i = 0; i < S.top; i++)
	{
		printf("%d ", S.s[i]);
	}
	printf("\n");
}
