/*
 tree.h -- binary search tree
 It does not allow that the tree has repeat element.
*/

#ifndef _TREE_H_
#define _TREE_H_

#include<stdboo.h>
#define SLEN 20

typedef struct item 
{
	char petname[SLEN];
	char petkind[SLEN];
} Item;

#define MAXITEMS 10

typedef struct trnode
{
	Item item;
	struct trnode *left; /* A pointer to the left node */
	struct trnode *right; /* Apointer to the right node */
} Trnode;

typedef struct tree
{
	Trnode *root; /* A pointer to the root node */
	int size; /* The number of items in tree */
}

/*
 initialize_tree(Tree *ptree):
 * operation : Initialize the tree to null
 * precondition: ptree pinter to a tree
 * postcondition: The tree is initialized to null
*/
void initialize_tree(Tree *ptree);

/*
 tree_is_empty(Tree *ptree):
 * operation: Be sure wether a tree is empty
 * precondition: ptree pointer to a tree
 * postcondition: if a tree is empty, return true, else return false.
*/
bool tree_is_empty(const Tree *ptree);

/*
 tree_is_full(Tree *ptree):
 * operation: Be sure wether a tree is full
 * precondition: ptree point to a tree
 * postcondition: if a tree is full, return true, else return false.
*/
bool tree_is_full(const Tree *ptree);
/*
 tree_item_count(const Tree *ptree):
 * operation: Be sure that the number of items in tree
 * precondition: ptree point to a tree
 * postcondition: Return the number if items in tree
*/
int tree_item_count(const Tree *ptree);

/*
 add_item(const Item *pi, Tree *ptree):
 * operation: Add a item in tree
 * precondition: ptree point to a tree, item is added into a tree
 * postcondition: if item is added sucessfully, retur true, else return false.
*/
bool add_item(const Item *pi, Tree *ptree);

/*
 delete_all(Tree *ptree):
 * operation: Delete all items in tree
 * precondition: ptree point to an initialized tree
 * postcondition: After deleting, the tree is empty
*/
void delete_all(Tree *ptree);

/*
 delete_item(const Item *item, Tree *ptree):
 * operation: Delete an item from a tree
 * precondition: item point to the element that will be deleted, ptree point to an initilized tree
 * postcondition: If item is deleted sucessfully, return true, else return false
*/
bool delete_item(const Item *item, Tree *ptree);

/*
 bool in_tree(const Item *item, Tree *ptree):
 * operation: Search item in tree
 * preconditon: item point to element whih will be searched, ptree point to an initilized tree
 * postcondition: if item in tree, return true, else return false
*/
bool in_tree(const Item *item, Tree *ptree);

/*
 traverse(const Tree *ptree, void(*pfun)(Item item)):
 * operation: Apply functuon to element in tree
 * precondition: ptree point to a tree, pfun point a function
 * postcondition: excute funcion in element of tree
*/
void traverse(const Tree *ptree, void(*pfun)(Item item));
#endif
