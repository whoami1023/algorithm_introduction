/*
 BinaryTree.h - The definition of binary tree
*/

#ifndef _BinaryTree_H_
#define _BinaryTree_H_

typedef int Item;
typedef struct node
{
	Item item;
	struct node *left;
	struct node *right;
} Trnode;

typedef struct tree
{
	Trnode * root;
	int size;
} Tree;
#endif
