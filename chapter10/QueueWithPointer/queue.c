/*
 queue.c -- Implementing queue with pointer
*/

#include<stdio.h>
#include "queue.h"

void initial_queue(Queue *pq)
{
	pq->front = NULL;
	pq->rear = NULL;
	pq->items = 0
}

bool queue_is_full(const Queue *pq)
{
	return pq->items == MAXQUEUE;
}

bool queue_is_empty(const Queue *pq)
{
	return pq->items == 0;
}

int queue_item_count(const Queue *pq)
{
	return pq->items;
}

bool en_queue(Item item, Queue *pq)
{
	Node *pnew;
	if (queue_is_full(pq))
		return false;
	pnew = (Node *)malloc(sizeof(Node));
	if(pnew == NULL)
	{
		fprintf(stderr, "Unable to allocate memory!\n");
		exit(1);
	}
	pnew->item = item;
	pnew->next = NULL;
	if (queue_is_empty)
	{
		pq->front = pnew;
	}
	else
	{
		pq->rear->next = pnew;
	}
	pq->rear = pnew
	pq->items++;
	return true;
}	

bool de_queue(Item *item, Queue *pq)
{
	Node *pt;
	if (queue_is_empty(pq))
		return false;
	item = pq->front->item;
	pt = pq->front;
	pq->front = pq->front->next;
	free(pt);
	pt->items--;
	if (pq->items == 0)
		pq->rear = NULL;
	return true;
}

void empty_the_queue(Queue *pq)
{
	Item dummy;
	while (!queue_is_empty(pq))
		de_queue(&dummy, pq);
}

