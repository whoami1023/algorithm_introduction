/*
 queue.h -- The interface of Queue
*/

#ifdef _QUEUE_H_
#define _QUEUE_H_

typedef int Item;

#define MAXQUEUE 10

/* Contructing node structure */
typedef struct node
{
	Item item;
	struct node *next;
} Node;

typedef struct queue
{
	Node *front; // The head of queue
	Node *rear;  // The rear of queue
	int items; // The numbers in the queue
} Queue;

void initialize_queue(Queue *pq);
bool queue_is_full(const Queue *pq);
bool queue_is_empty(const Queue *pq);
int queue_item_count(const Queue *pq);
void en_queue(Item item, Queue *pq);
bool de_queue(Item *item, Queue *pq);
void empty_the_queue(Queue *pq);

#endif
