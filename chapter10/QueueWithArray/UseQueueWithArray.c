/*
UseQueueWithArray.c -- Testing the interface of queue implemented with array
*/

#include<stdio.h>
#include<stdlib.h>
#include "Queue_With_Array.h"

#define DEBUG 0

int main(void)
{
	Queue *q;
	q = (Queue *)malloc(sizeof(Queue));
	initial_queue(q);
	#if DEBUG == 1
	q->head++;
	printf("%d\n", q->head); 
	#endif
	enqueue(q, 3);
	printf("[Enqueue]:%d\n", q->q[q->tail-1]);
	enqueue(q, 4);
	printf("[Enqueue]:%d\n", q->q[q->tail-1]);
	printf("[Dequeue]:%d\n", dequeue(q));
	printf("[Dequeue]:%d\n", dequeue(q));
	printf("[Dequeue]:%d\n", dequeue(q));
	return 0;
}
