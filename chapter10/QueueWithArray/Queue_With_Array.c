#include<stdio.h>
#include<stdlib.h>
#include "Queue_With_Array.h"

void initial_queue(Queue *Q)
{
	Q->head = Q->tail = 0;
}

bool queue_is_empty(const Queue *Q)
{
	return Q->head == Q->tail;
}

bool queue_is_full(const Queue *Q)
{
	return ((Q->tail + 1) % MAXLENGTH) == Q->head;
}

void enqueue(Queue *Q, Item item)
{
	if (queue_is_full(Q)) 
	{
		fprintf(stderr, "The queue is full!\n");
		exit(1);
	}
	Q->q[Q->tail] = item;
	Q->tail = ++Q->tail % MAXLENGTH;
}
Item dequeue(Queue *Q)
{
	Item item;
	if (queue_is_empty(Q))
	{
		fprintf(stderr, "The queue is empty!\n");
		exit(1);
	}
	item = Q->q[Q->head];
	Q->head = ++Q->head % MAXLENGTH;
	return item;
}
