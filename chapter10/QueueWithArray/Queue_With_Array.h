#ifndef _Queue_With_Array_H_
#define _Queue_With_Array_H_

#include<stdbool.h>
#define MAXLENGTH 20
typedef int Item;

typedef struct queue{
	Item q[MAXLENGTH];
	int head;
	int tail;	
} Queue;
void initial_queue(Queue *Q);
bool queue_is_empty(const Queue *Q);
bool queue_is_full(const Queue *Q);
void enqueue(Queue *Q, Item item);
Item dequeue(Queue *Q);

#endif
